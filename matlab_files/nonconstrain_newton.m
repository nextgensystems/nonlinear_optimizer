clc;
clear;
syms xt yt zt x y z;
%control variables x1, y1, x2, y2
%constants xc1, yc1, xc2, yc2, b0, h0, l0,
a0 = 1.62;
b0 = 0.72717;
c0 = 0.5;
d0 = 0.15;
h0 = 1.2;
l0 = 7.85;
xc1 = 0;
yc1 = 0;
xc2 = sqrt(l0^2-h0^2);
yc2 = 0;

%x1 = 0.05;
%y1 = 0.05;
%x2 = 0.01;
%y2 = 0.005;
xr = 6.05594;
yr = 0.54294;
xb = 6.65812;
yb = 0.665876;
zb = 1.30879;


eq1 = xt - xr - (a0/b0)*(xb-xr);
eq2 = yt - yr - (a0/b0)*(yb-yr);
eq3 = zt - h0 - (a0/b0)*(zb-h0);
eq4 = (xb-xr)*(x-xt) + (yb-yr)*(y-yt) + (zb-h0)*(z-zt);
eq5 = (xb-xr)*(y-yt) - (x-xt)*(yb-yr);
eq6 = (x-xb)^2 + (y-yb)^2 + (z-zb)^2 - (a0-b0)^2 - c0^2;

system = [eq1; eq2; eq3; eq4; eq5; eq6];
jacobian_matrix = jacobian(system, [xt, yt, zt, x, y, z])
max = 1000;
xk_current = [xc2+d0+a0; 0; h0; xc2+d0+a0; 0; h0-c0];
delta = 0.000001;
epsilon = 0.000001;

for k = 1:max
    jacobian_matrix_numeric = double(subs(jacobian_matrix, {xt, yt, zt, x, y, z}, {xk_current(1), xk_current(2), xk_current(3), xk_current(4), xk_current(5), xk_current(6)}))
    system_numeric = double(subs(system, {xt, yt, zt, x, y, z}, {xk_current(1), xk_current(2), xk_current(3), xk_current(4), xk_current(5), xk_current(6)}));
    %xk_next = xk_current - inv(transpose(jacobian_matrix_numeric)*jacobian_matrix_numeric)*transpose(jacobian_matrix_numeric)*system_numeric
    xk_next = xk_current - pinv(jacobian_matrix_numeric)*system_numeric;
    %xk_next = xk_current - system_numeric*pinv(jacobian_matrix_numeric);
    err = norm(xk_next-xk_current);
    relerr = err/(norm(xk_next)+eps);
    xk_current = xk_next;
    if (err < delta || relerr < delta || norm(system_numeric) < epsilon)
        break;
    end
    %norm(system_numeric)
    %k
end
xk_current
%double(asin((xk_current(2)-(y2+yc2))/b0))
%double(asin((xk_current(3)-h0)/b0))
